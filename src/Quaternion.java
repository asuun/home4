import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

   // TODO!!! Your fields here!
    private final double a,b,c,d;
   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */

   public Quaternion (double a, double b, double c, double d) {
       this.a=a;
       this.b=b;
       this.c=c;
       this.d=d;
      // TODO!!! Your constructor here!
   }

    /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.a; // TODO!!!
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return this.b; // TODO!!!
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return this.c; // TODO!!!
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return this.d; // TODO!!!
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
		StringBuilder Quat=new StringBuilder();
		Quat.append(this.a);

		if(this.b>=0){
			Quat.append("+"+b+"i");
		}else{
			Quat.append(b+"i");
		}

		if(this.c>=0){
			Quat.append("+"+c+"j");
		}else{
			Quat.append(c+"j");
		}

		if(this.d>=0){
			Quat.append("+"+d+"k");
		}else{
			Quat.append(d+"k"); }

       return Quat.toString(); // TODO!!!
   }

    /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
    public static Quaternion valueOf (String s) {
		double r, i,j,k;
		int plus=s.substring(0,s.length()).indexOf('+');
		int minus=s.substring(1,s.length()).indexOf('-');
		int op=0;

		if(minus<0){
		    op=plus;
		}else if(plus<0){
		    op=minus;
		}else if(minus<plus){
			op=minus;
		}else if (minus>plus){
			op=plus;
		} else{
			throw new IllegalArgumentException("Invalid arguments");
			}
		/*
		if parsing doubles between imaginary components and real part fails, IllegalArgumentException is thrown
		 */
		try{
			k=Double.parseDouble(s.substring(s.indexOf('j')+1,s.indexOf('k')));
			j=Double.parseDouble(s.substring(s.indexOf('i')+1,s.indexOf('j')));
			i=Double.parseDouble(s.substring(op+1,s.indexOf('i')));
			r=Double.parseDouble(s.substring(0,op));
		}catch (NumberFormatException e){
			throw new IllegalArgumentException("Invalid arguments");
		};

		return new Quaternion(r,i,j,k); // TODO!!!
    }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
	public Object clone() throws CloneNotSupportedException {
	    return new Quaternion(this.a,this.b,this.c, this.d ); // TODO!!!
	}

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
    public boolean isZero() {
		double difa=Math.abs(this.a);
		double difb=Math.abs(this.b);
		double difc=Math.abs(this.c);
		double difd=Math.abs(this.d);

	//if component abs value is less than 0.000001 it will count as zero component
	    if(difa<1E-7 && difb<1E-7 && difc<1E-7 && difd<1E-7){
		   return true;
		}else{
		   return false;
	    }

    }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
	public Quaternion conjugate() {
	return new Quaternion(this.a,-this.b,-this.c,-this.d); // TODO!!!
	}

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
	public Quaternion opposite() {
		return new Quaternion(-this.a,-this.b,-this.c,-this.d); // TODO!!!
	}

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
	public Quaternion plus (Quaternion q) {
		return new Quaternion(this.a+q.a,this.b+q.b,this.c+q.c,this.d+q.d); // TODO!!!
	}

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
   	double q1[];
   	double q2[];
   	q1=new double[]{this.a,this.b,this.c,this.d};
   	q2=new double[]{q.a,q.b,q.c,q.d};

   	double buffer[][];  // buffer for first multiplication
   	double matrix[][];  // products
   	double imag[][];    // imaginary part matrix
	imag=new double[][]{{1,1,1,1}, {-1,1,-1,1},{-1,1,1,-1},{-1,-1,1,1}};
	buffer=new double[4][4];
	matrix=new double[4][4];
	int enamulti=0;     //enable multiplier-> counter for the right matrix index
	   // imaginary part
	   for(int i=0;i<4;i++){
		   for(int j=0;j<4;j++){
			   buffer[i][j]=q1[j]*imag[j][i];
		   }
	   }

	   //matrix multiplication
	    for(int i=0;i<4;i++){
			for(int j=0;j<4;j++){
			    // extra requirement for the right result
			    if(i*j==2){
				    enamulti=3;
				    matrix[i][j]=q2[enamulti]*buffer[i][j];
			    }else{
				    enamulti=Math.abs(i-j);
				    matrix[i][j]=q2[enamulti]*buffer[i][j];
			    }
		    }
	    }
		double r=0.0;
		double i=0.0;
		double j=0.0;
		double k=0.0;

		// summing up the products
		for(int sum=0; sum<4;sum++){
			r=r+matrix[0][sum];
			i=i+matrix[1][sum];
			j=j+matrix[2][sum];
			k=k+matrix[3][sum];
        }
        return new Quaternion(r,i,j,k); // TODO!!!
    }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
    public Quaternion times (double r) {
   	    return new Quaternion(r*this.a,r*this.b,r*this.c,r*this.d); // TODO!!!
    }

    /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
    public Quaternion inverse() {
		// if matrix is zero, exception is thrown
		if(this.isZero()==true){
			throw new IllegalArgumentException(this+"is zero");
		}else{
			return new Quaternion(this.a/(a*a+b*b+c*c+d*d),-this.b/(a*a+b*b+c*c+d*d),
			        -this.c/(a*a+b*b+c*c+d*d),-this.d/(a*a+b*b+c*c+d*d)); // TODO!!!
		}
    }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
	public Quaternion minus (Quaternion q) {
		return new Quaternion(this.a-q.a,this.b-q.b,this.c-q.c,this.d-q.d); // TODO!!!
	}

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
	public Quaternion divideByRight (Quaternion q) {
		return this.times(q.inverse()); // TODO!!!
	}

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
	public Quaternion divideByLeft (Quaternion q) {
		return q.inverse().times(this); // TODO!!!
	}
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
	@Override
	public boolean equals (Object qo) {
		//cast Quaternion for object
		Quaternion q1=(Quaternion)qo;
		double difa=Math.abs(q1.a-this.a);
		double difb=Math.abs(q1.b-this.b);
		double difc=Math.abs(q1.c-this.c);
		double difd=Math.abs(q1.d-this.d);

	    if(difa<1E-7 && difb<1E-7 && difc<1E-7 && difd<1E-7d){
	    	return true;
	    }else{
	    	return false;
	    }
	}

    /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
	public Quaternion dotMult (Quaternion q) {
		Quaternion p=new Quaternion(this.a,this.b,this.c,this.d);
		return p.conjugate().times(q).plus(q.conjugate().times(p)).times(0.5); // TODO!!!
	}

    /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
	@Override
	public int hashCode() {
		Double hash=this.a*this.b+this.c*this.d;
		int hashCode=hash.intValue();   //double to int

		  return hashCode; // TODO!!!
	}

    /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
	public double norm() {
		return Math.sqrt(a*a+b*b+c*c+d*d); // TODO!!!
	}

    /** Main method for testing purposes.
    * @param arg command line parameters
    */
	public static void main (String[] arg) {
		Quaternion arv1 = new Quaternion (1., 1., -1., 1.);
		String string=arv1.toString();
		if (arg.length > 0)
		 arv1 = valueOf (arg[0]);
		System.out.println ("first: " + arv1.toString());

		System.out.println("valueof:"+ valueOf(string));
		System.out.println ("real: " + arv1.getRpart());
		System.out.println ("imagi: " + arv1.getIpart());
		System.out.println ("imagj: " + arv1.getJpart());
		System.out.println ("imagk: " + arv1.getKpart());
		System.out.println ("isZero: " + arv1.isZero());
		System.out.println ("conjugate: " + arv1.conjugate());
		System.out.println ("opposite: " + arv1.opposite());
		System.out.println ("hashCode: " + arv1.hashCode());
		Quaternion res = null;
		try {
		 res = (Quaternion)arv1.clone();
		} catch (CloneNotSupportedException e) {};
		System.out.println ("clone equals to original: " + res.equals (arv1));
		System.out.println ("clone is not the same object: " + (res!=arv1));
		System.out.println ("hashCode: " + res.hashCode());
		res = valueOf (arv1.toString());
		System.out.println ("string conversion equals to original: "
		 + res.equals (arv1));
		Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
		if (arg.length > 1)
		 arv2 = valueOf (arg[1]);
		System.out.println ("second: " + arv2.toString());
		System.out.println ("hashCode: " + arv2.hashCode());
		System.out.println ("equals: " + arv1.equals (arv2));
		res = arv1.plus (arv2);
		System.out.println ("plus: " + res);
		System.out.println ("times: " + arv1.times (arv2));
		System.out.println ("minus: " + arv1.minus (arv2));
		double mm = arv1.norm();
		System.out.println ("norm: " + mm);
		System.out.println ("inverse: " + arv1.inverse());
		System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
		System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
		System.out.println ("dotMult: " + arv1.dotMult (arv2));

	}
}
// end of file
